class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.integer :row
      t.string :email

      t.timestamps
    end
  end
end
