Rails.application.routes.draw do
  devise_for :users

  get 'oauth2_callbacks', to: 'oauth2_callbacks#create'

  put :sheets, to: 'sheets#update'

  root to: 'sheets#index'
end
