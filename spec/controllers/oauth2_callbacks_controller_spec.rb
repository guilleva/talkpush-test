require 'rails_helper'

RSpec.describe Oauth2CallbacksController, type: :controller do
  describe 'GET #create' do
    context 'unauthenticated user' do
      it 'returns http success' do
        get :create
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'authenticated user' do
      let(:user) { create :user }
      let(:google_api_authorized) { true }
      let(:credentials) { instance_double(GoogleSheets::UserCredentials) }

      before do
        sign_in user
        allow(controller).to receive(:user_api_credentials).and_return(
          credentials
        )
        allow(credentials).to receive(:handle_auth_callback).and_return('/')
      end

      it 'returns http success' do
        get :create
        expect(response).to redirect_to('/')
      end
    end
  end
end
