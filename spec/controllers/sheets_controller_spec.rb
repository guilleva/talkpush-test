require 'rails_helper'

RSpec.describe SheetsController, type: :controller do
  describe 'GET #index' do
    context 'unauthenticated user' do
      it 'redirects to login page' do
        get :index
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'authenticated user' do
      let(:user) { create :user }
      let(:google_api_authorized) { true }
      let(:credentials) { instance_double(GoogleSheets::UserCredentials) }

      before do
        sign_in user
        allow(controller).to receive(:user_api_credentials).and_return(
          credentials
        )
        allow(credentials).to receive(:authorized?).and_return(google_api_authorized)
      end

      it 'returns http success' do
        get :index
        expect(response).to have_http_status(:success)
      end

      context "when not authorized on Google's API" do
        let(:google_api_authorized) { false }

        it 'returns http success' do
          expect(credentials).to receive(:autorization_url)
            .with('http://test.host/')
            .and_return(
              'http://www.google.com/oauthendpoint'
            )
          get :index
          expect(response).to redirect_to('http://www.google.com/oauthendpoint')
        end
      end
    end
  end

  describe 'PUT #update' do
    context 'unauthenticated user' do
      it 'redirects to login page' do
        put :update
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'authenticated user' do
      let(:user) { create :user }
      let(:google_api_authorized) { true }
      let(:credentials) { instance_double(GoogleSheets::UserCredentials) }
      let(:processor) { instance_double(SpreadsheetProcessor, new_records_count: new_records_count, call: true) }
      let(:new_records_count) { 0 }

      before do
        sign_in user
        allow(controller).to receive(:user_api_credentials).and_return(
          credentials
        )
        allow(controller).to receive(:processor).and_return(processor)
        allow(credentials).to receive(:authorized?).and_return(google_api_authorized)
        allow(credentials).to receive(:credentials).and_return(:some_valid_credentials)
      end

      it 'redirects to the root page and sets the correct flash message' do
        put :update
        expect(response).to redirect_to(root_path)
        expect(flash[:notice]).to eql 'I am Sorry! No new candidates!'
      end

      context 'when one candidate was processed' do
        let(:new_records_count) { 1 }

        it 'returns http success' do
          put :update
          expect(response).to redirect_to(root_path)
          expect(flash[:notice]).to eql '1 new candidate processed'
        end
      end

      context 'when more than one candidates were processed' do
        let(:new_records_count) { 3 }

        it 'returns http success' do
          put :update
          expect(response).to redirect_to(root_path)
          expect(flash[:notice]).to eql '3 new candidates processed'
        end
      end
    end
  end
end
