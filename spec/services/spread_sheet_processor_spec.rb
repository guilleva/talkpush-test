require 'rails_helper'

RSpec.describe SpreadsheetProcessor, type: :model do
  let(:processor) { described_class.new(:credentials) }

  let(:sheet) { instance_double(GoogleSheets::Spreadsheet, values: rows) }
  let(:talkpush_client) { instance_double(TalkpushClient, create_candidate: true) }
  let(:rows) { [] }

  before do
    allow(processor).to receive(:sheet).and_return(sheet)
    allow(processor).to receive(:talkpush_client).and_return(talkpush_client)
  end

  describe 'call' do
    it 'does nothing when no rows are returned' do
      expect { processor.call }.not_to change(Candidate, :count)
    end

    context 'when it returns a record' do
      let(:rows) do
        [['6/15/2019 10:01:27', 'Fulanito', 'de tal',
          'fulanito@cr.com', '22222222']]
      end

      it 'creates a candidate record' do
        expect(talkpush_client).to receive(:create_candidate).with(
          email: 'fulanito@cr.com',
          first_name: 'Fulanito',
          last_name: 'de tal',
          source: "guillermo's code test",
          user_phone_number: '22222222'
        )
        expect { processor.call }.to change(Candidate, :count).by(1)
      end
    end

    context 'a candiate exists' do
      before { create :candidate, row: 20 }

      it 'requests the correct rage' do
        expect(sheet).to receive(:values).with('A21:E')
        processor.call
      end
    end
  end
end
