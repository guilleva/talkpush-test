FactoryBot.define do
  factory :user do
    email { |i| "test#{i}@localhost.local" }
    password { "SomeStrongPassword!" }
    password_confirmation { "SomeStrongPassword!" }
  end
end
