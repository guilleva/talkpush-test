class Candidate < ApplicationRecord
  validates :email, presence: true
  validates :row, presence: true, uniqueness: true
end
