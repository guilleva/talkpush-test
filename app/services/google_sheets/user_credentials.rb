require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'

module GoogleSheets
  class UserCredentials
    OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'.freeze
    CREDENTIALS_PATH = Rails.root.join('config', 'credentials.json').freeze

    TOKEN_PATH = Rails.root.join(ENV.fetch('GOOGLE_TOKEN_PATH')).freeze.freeze
    SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS_READONLY

    attr_reader :user_id, :callback_url, :request

    def initialize(user_id, request:, callback_url: nil)
      @user_id = user_id
      @callback_url = callback_url
      @request = request
    end

    def authorized?
      credentials.present?
    end

    def credentials
      @credentials ||= authorizer.get_credentials(user_id, request)
    end

    def autorization_url(redirect_url)
      authorizer.get_authorization_url(
        login_hint: user_id, request: request, base_url: redirect_url
      )
    end

    def handle_auth_callback
      Google::Auth::WebUserAuthorizer.handle_auth_callback_deferred(
        request
      )
    end

    def authorizer
      @authorizer ||= begin
        client_id = Google::Auth::ClientId.from_file(CREDENTIALS_PATH)
        token_store = Google::Auth::Stores::FileTokenStore.new(file: TOKEN_PATH)
        Google::Auth::WebUserAuthorizer.new(
          client_id, SCOPE, token_store, callback_url
        )
      end
    end
  end
end
