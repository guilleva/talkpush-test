
require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'

module GoogleSheets
  class Spreadsheet
    attr_reader :credentials, :spreadsheet_id
    APPLICATION_NAME = 'TalkPush Test'.freeze

    def initialize(credentials, spreadsheet_id)
      @credentials = credentials
      @spreadsheet_id = spreadsheet_id
    end

    def values(range)
      service.get_spreadsheet_values(spreadsheet_id, range).try(:values)
    end

    def service
      @service ||= begin
        service = Google::Apis::SheetsV4::SheetsService.new
        service.client_options.application_name = APPLICATION_NAME
        service.authorization = credentials
        service
      end
    end
  end
end
