class SpreadsheetProcessor
  class ReadError < StandardError; end

  attr_reader :credentials, :new_records_count

  SPREADSHEET = ENV.fetch('SPREADSHEET').freeze

  def initialize(credentials)
    @credentials = credentials
    @new_records_count = 0
  end

  def call
    raise ReadError if sheet.nil?

    row_number = Candidate.order(row: :desc).limit(1).pluck(:row).first || 1
    next_batch_range = "A#{row_number + 1}:E"
    (sheet.values(next_batch_range) || []).each do |(_, first_name, last_name, email, phone_number)|
      if email
        Candidate.create!(row: row_number + 1, email: email)
        talkpush_client.create_candidate(
          first_name: first_name,
          last_name: last_name,
          email: email,
          user_phone_number: phone_number,
          source: 'guillermo\'s code test'
        )
        @new_records_count += 1
      end
      row_number += 1
    end
  rescue Google::Apis::ClientError
    raise ReadError
  end

  def sheet
    @sheet ||= GoogleSheets::Spreadsheet.new(
      credentials,
      SPREADSHEET
    )
  end

  def talkpush_client
    @talkpush_client ||= TalkpushClient.new
  end
end
