require 'faraday'

class TalkpushClient
  TALKPUSH_API_BASE_URL = ENV.fetch('TALKPUSH_API_BASE_URL').freeze
  TALKPUSH_API_KEY = ENV.fetch('TALKPUSH_API_KEY').freeze
  TALKPUSH_API_SECRET = ENV.fetch('TALKPUSH_API_SECRET').freeze
  TALKPUSH_CAMPAIGN_ID = ENV.fetch('TALKPUSH_CAMPAIGN_ID').freeze

  def create_candidate(campaign_invitation)
    connection.post "/api/talkpush_services/campaigns/#{TALKPUSH_CAMPAIGN_ID}/campaign_invitations" do |request|
      request.headers['Content-Type'] = 'application/json'
      request.headers['Accept'] = 'application/json'
      request.body = credentials_params.merge(
        campaign_invitation: campaign_invitation
      ).to_json
    end
  end

  def candidates
    connection.get '/api/talkpush_services/campaign_invitations.json',
                   credentials_params.merge(
                     filter: { campaign_id: TALKPUSH_CAMPAIGN_ID }
                   )
  end

  private

  def credentials_params
    {
      api_key: TALKPUSH_API_KEY,
      api_secret: TALKPUSH_API_SECRET
    }
  end

  def connection
    @connection ||= Faraday.new(url: TALKPUSH_API_BASE_URL) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests and responses to $stdout
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end
end
