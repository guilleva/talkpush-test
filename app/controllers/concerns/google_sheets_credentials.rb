module GoogleSheetsCredentials
  extend ActiveSupport::Concern

  def user_api_credentials
    @user_api_credentials ||= GoogleSheets::UserCredentials.new(
      current_user.id, request: request,
                       callback_url: oauth2_callbacks_url
    )
  end

  def authorize_google_sheets_api!
    return if user_api_credentials.authorized?

    redirect_to user_api_credentials.autorization_url(root_url)
  end
end
