require 'googleauth'
require 'googleauth/web_user_authorizer'

class Oauth2CallbacksController < ApplicationController
  include GoogleSheetsCredentials

  def create
    target_url = user_api_credentials.handle_auth_callback

    redirect_to target_url
  end
end
