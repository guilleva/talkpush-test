class SheetsController < ApplicationController
  include GoogleSheetsCredentials

  before_action :authorize_google_sheets_api!

  def index; end

  def update
    processor.call
    if processor.new_records_count.positive?
      flash[:notice] = "#{view_context.pluralize(processor.new_records_count, 'new candidate')} processed"
    else
      flash[:notice] = 'I am Sorry! No new candidates!'
    end
  rescue SpreadsheetProcessor::ReadError
    flash[:error] = "An error ocurred trying to read the Spreadsheet"
  ensure
    redirect_to root_path
  end

  private

  def processor
    @processor ||= SpreadsheetProcessor.new(user_api_credentials.credentials)
  end
end
